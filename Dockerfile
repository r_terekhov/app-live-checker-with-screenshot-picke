FROM registry.gitlab.ivoice.online/ivoice-platform/deploy-images/maven-nexus-settings:latest as builder
ADD . /app
WORKDIR /app
RUN ["mvn", "package", "-DskipTests=true"]

FROM anapsix/alpine-java:8_jdk

#setup MSK timezone
RUN apk update && \
    apk add tzdata && \
    cp /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    echo "Etc/UTC" >  /etc/timezone && \
    apk del tzdata

WORKDIR /app
COPY --from=builder /app/target/screenshot-live-checker.jar .
ENV LOG_LEVEL_APP=INFO
ENV LOG_LEVEL_ROOT=INFO
ADD ./docker-entry.sh .
RUN chmod +x ./docker-entry.sh
CMD ./docker-entry.sh
EXPOSE 8555