package tech.ivoice.screenshot.livechecker.service;

public interface AppLiveCheckerService {

    void checkApp();

    void setup();
}
