package tech.ivoice.screenshot.livechecker.service;

public interface NotificationService {

    void sendErrorNotificationSlack(String errorMessage, String channel);
}
