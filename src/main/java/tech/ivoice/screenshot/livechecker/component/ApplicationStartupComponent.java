package tech.ivoice.screenshot.livechecker.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import tech.ivoice.screenshot.livechecker.service.AppLiveCheckerService;
import tech.ivoice.screenshot.livechecker.utils.Utils;

import static java.text.MessageFormat.format;

@Component
public class ApplicationStartupComponent implements ApplicationListener<ApplicationReadyEvent> {
    private Logger logger = LoggerFactory.getLogger(ApplicationStartupComponent.class);
    private final AppLiveCheckerService appLiveCheckerService;

    public ApplicationStartupComponent(AppLiveCheckerService appLiveCheckerService) {
        this.appLiveCheckerService = appLiveCheckerService;
    }


    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        appLiveCheckerService.setup();
    }
}